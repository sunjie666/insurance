package com.haohuo.service.serviceImpl;

import com.haohuo.beans.*;
import com.haohuo.mapper.*;
import com.haohuo.service.InsuranceService;
import com.haohuo.utils.BeanMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by fsj on 2019/12/3.
 */
@Service
public class InsuranceServiceImpl implements InsuranceService {
    @Autowired
    private InsuranceSchemeMapper insuranceSchemeMapper;
    @Autowired
    private InsurancePlanMapper insurancePlanMapper;
    @Autowired
    private InsuranceMapper insuranceMapper;
    @Autowired
    private InsuranceRecordMapper insuranceRecordMapper;
    @Autowired
    private InsuranceUserMapper insuranceUserMapper;
    @Autowired
    private InsuranceProjectMapper insuranceProjectMapper;
    @Override
    public int createScheme(InsuranceScheme scheme) {
        return insuranceSchemeMapper.insertSelective(scheme);
    }

    @Override
    public int createPlan(InsurancePlan plan) {
        return insurancePlanMapper.insertSelective(plan);
    }

    @Override
    public int createInsurance(Insurance insurance) {
        return insuranceMapper.insertSelective(insurance);
    }

    @Override
    public List<BeanMap> getSchemeListByWhere(Map<String, Object> map) {
        return insuranceSchemeMapper.selectSchemeListByWhere(map);
    }

    @Override
    public int getSchemeListCount(Map<String, Object> map) {
        return insuranceSchemeMapper.selectSchemeListCount(map);
    }

    @Override
    public int modifyScheme(InsuranceScheme scheme) {
        return insuranceSchemeMapper.updateByPrimaryKeySelective(scheme);
    }

    @Override
    public InsuranceScheme getSchemeInfoById(Integer isId) {
        return insuranceSchemeMapper.selectByPrimaryKey(isId);
    }

    @Override
    public List<InsurancePlan> getPlanBySchemeId(Integer isId) {
        return insurancePlanMapper.selectPlanBySchemeId(isId);
    }

    @Override
    public List<Insurance> getInsuranceByPlanId(Integer ipId) {
        return insuranceMapper.selectInsuranceByPlanId(ipId);
    }

    @Override
    public int modifyInsurance(Insurance insurance) {
        return insuranceMapper.updateByPrimaryKeySelective(insurance);
    }

    @Override
    public int modifyPlan(InsurancePlan plan) {
        return insurancePlanMapper.updateByPrimaryKeySelective(plan);
    }

    @Override
    public int delSChemeById(Integer isId) {
        return insuranceSchemeMapper.deletShcenmeAndPlanAndIns(isId);
    }

    @Override
    public List<BeanMap> getUserListByUserName(String userName) {
        return insuranceSchemeMapper.selectUserListByUserName(userName);
    }

    @Override
    public List<InsuranceScheme> getSchemeListBySchemeName(String schemeName) {
        return insuranceSchemeMapper.selectSchemeListBySchemeName(schemeName);
    }

    @Override
    public List<BeanMap> getOptByKeyWords(Map optMap) {
        return insuranceSchemeMapper.selectOptByKeyWords(optMap);
    }

    @Override
    public List<InsurancePlan> getPlanByIsId(Integer isId) {
        return insurancePlanMapper.selectPlanByIsId(isId);
    }

    @Override
    public int createRecord(InsuranceRecord record) {
        return insuranceRecordMapper.insertSelective(record);
    }

    @Override
    public int cutBuyPlanRecord(Map<String, Object> recordMap) {
        return insuranceRecordMapper.updateByUserIdAndOptId(recordMap);
    }

    @Override
    public int createInsuranceUser(InsuranceUser insuranceUser) {
        return insuranceUserMapper.insertSelective(insuranceUser);
    }

    @Override
    public List<BeanMap> getUserInsuranceByUserId(Integer mpId) {
        return insuranceUserMapper.selectByMpId(mpId);
    }

    @Override
    public int createInsuranceProject(InsuranceProject insuranceProject) {
        return insuranceProjectMapper.insertSelective(insuranceProject);
    }
}
