package com.haohuo.service;

import com.haohuo.beans.*;
import com.haohuo.utils.BeanMap;

import java.util.List;
import java.util.Map;

/**
 * Created by HaoHuo on 2019/12/3.
 */
public interface InsuranceService {
    int createScheme(InsuranceScheme scheme);

    int createPlan(InsurancePlan plan);

    int createInsurance(Insurance insurance);

    List<BeanMap> getSchemeListByWhere(Map<String, Object> map);

    int getSchemeListCount(Map<String, Object> map);

    int modifyScheme(InsuranceScheme scheme);

    InsuranceScheme getSchemeInfoById(Integer isId);

    List<InsurancePlan> getPlanBySchemeId(Integer isId);

    List<Insurance> getInsuranceByPlanId(Integer ipId);

    int modifyInsurance(Insurance insurance);

    int modifyPlan(InsurancePlan plan);

    int delSChemeById(Integer isId);

    List<BeanMap> getUserListByUserName(String userName);

    List<InsuranceScheme> getSchemeListBySchemeName(String schemeName);

    List<BeanMap> getOptByKeyWords(Map optMap);

    List<InsurancePlan> getPlanByIsId(Integer isId);

    int createRecord(InsuranceRecord record);

    int cutBuyPlanRecord(Map<String, Object> recordMap);


    int createInsuranceUser(InsuranceUser insuranceUser);

    List<BeanMap> getUserInsuranceByUserId(Integer mpId);

    int createInsuranceProject(InsuranceProject insuranceProject);
}
