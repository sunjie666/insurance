package com.haohuo.mapper;

import com.haohuo.beans.InsurancePlan;

import java.util.List;

public interface InsurancePlanMapper {
    int deleteByPrimaryKey(Integer ipId);

    int insert(InsurancePlan record);

    int insertSelective(InsurancePlan record);

    InsurancePlan selectByPrimaryKey(Integer ipId);

    int updateByPrimaryKeySelective(InsurancePlan record);

    int updateByPrimaryKey(InsurancePlan record);

    List<InsurancePlan> selectPlanBySchemeId(Integer isId);

    List<InsurancePlan> selectPlanByIsId(Integer isId);
}