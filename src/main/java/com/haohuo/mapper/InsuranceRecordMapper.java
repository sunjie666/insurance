package com.haohuo.mapper;

import com.haohuo.beans.InsuranceRecord;

import java.util.Map;

public interface InsuranceRecordMapper {
    int deleteByPrimaryKey(Integer irId);

    int insert(InsuranceRecord record);

    int insertSelective(InsuranceRecord record);

    InsuranceRecord selectByPrimaryKey(Integer irId);

    int updateByPrimaryKeySelective(InsuranceRecord record);

    int updateByPrimaryKey(InsuranceRecord record);

    int updateByUserIdAndOptId(Map<String, Object> recordMap);
}