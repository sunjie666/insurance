package com.haohuo.mapper;

import com.haohuo.beans.InsuranceProject;

public interface InsuranceProjectMapper {
    int deleteByPrimaryKey(Integer prId);

    int insert(InsuranceProject record);

    int insertSelective(InsuranceProject record);

    InsuranceProject selectByPrimaryKey(Integer prId);

    int updateByPrimaryKeySelective(InsuranceProject record);

    int updateByPrimaryKey(InsuranceProject record);
}