package com.haohuo.mapper;

import com.haohuo.beans.Insurance;

import java.util.List;

public interface InsuranceMapper {
    int deleteByPrimaryKey(Integer inId);

    int insert(Insurance record);

    int insertSelective(Insurance record);

    Insurance selectByPrimaryKey(Integer inId);

    int updateByPrimaryKeySelective(Insurance record);

    int updateByPrimaryKey(Insurance record);

    List<Insurance>  selectInsuranceByPlanId(Integer ipId);
}