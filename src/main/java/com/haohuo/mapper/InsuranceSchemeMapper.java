package com.haohuo.mapper;

import com.haohuo.beans.InsuranceScheme;
import com.haohuo.utils.BeanMap;

import java.util.List;
import java.util.Map;


public interface InsuranceSchemeMapper {
    int deleteByPrimaryKey(Integer isId);

    int insert(InsuranceScheme record);

    int insertSelective(InsuranceScheme record);

    InsuranceScheme selectByPrimaryKey(Integer isId);

    int updateByPrimaryKeySelective(InsuranceScheme record);

    int updateByPrimaryKey(InsuranceScheme record);

    List<BeanMap> selectSchemeListByWhere(Map<String, Object> map);

    int selectSchemeListCount(Map<String, Object> map);

    int deletShcenmeAndPlanAndIns(Integer isId);

    List<BeanMap> selectUserListByUserName(String userName);

    List<InsuranceScheme> selectSchemeListBySchemeName(String schemeName);

    List<BeanMap> selectOptByKeyWords(Map optMap);
}