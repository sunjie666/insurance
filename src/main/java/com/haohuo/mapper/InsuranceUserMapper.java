package com.haohuo.mapper;

import com.haohuo.beans.InsuranceUser;
import com.haohuo.utils.BeanMap;

import java.util.List;

public interface InsuranceUserMapper {
    int deleteByPrimaryKey(Integer iuId);

    int insert(InsuranceUser record);

    int insertSelective(InsuranceUser record);

    InsuranceUser selectByPrimaryKey(Integer iuId);

    int updateByPrimaryKeySelective(InsuranceUser record);

    int updateByPrimaryKey(InsuranceUser record);

    List<BeanMap> selectByMpId(Integer mpId);
}