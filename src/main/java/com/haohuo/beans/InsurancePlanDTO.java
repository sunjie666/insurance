package com.haohuo.beans;

import java.util.List;

/**
 * Created by HaoHuo on 2019/12/4.
 */
public class InsurancePlanDTO extends InsurancePlan {
    List<Insurance>insuranceList;

    public List<Insurance> getInsuranceList() {
        return insuranceList;
    }

    public void setInsuranceList(List<Insurance> insuranceList) {
        this.insuranceList = insuranceList;
    }
}
