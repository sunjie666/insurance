package com.haohuo.beans;

import java.io.Serializable;

public class InsurancePlan implements Serializable {
    private Integer ipId;

    private Integer ipIsId;

    private String ipPlan;

    private Integer ipPrice;

    private String ipCreatTime;

    private String ipServiceContent;

    private Integer ipState;

    private String ipStateTime;
    private String ipRemark;

    private static final long serialVersionUID = 1L;

    public Integer getIpId() {
        return ipId;
    }

    public void setIpId(Integer ipId) {
        this.ipId = ipId;
    }

    public Integer getIpIsId() {
        return ipIsId;
    }

    public void setIpIsId(Integer ipIsId) {
        this.ipIsId = ipIsId;
    }

    public String getIpPlan() {
        return ipPlan;
    }

    public void setIpPlan(String ipPlan) {
        this.ipPlan = ipPlan;
    }

    public Integer getIpPrice() {
        return ipPrice;
    }

    public void setIpPrice(Integer ipPrice) {
        this.ipPrice = ipPrice;
    }

    public String getIpCreatTime() {
        return ipCreatTime;
    }

    public void setIpCreatTime(String ipCreatTime) {
        this.ipCreatTime = ipCreatTime;
    }

    public String getIpServiceContent() {
        return ipServiceContent;
    }

    public void setIpServiceContent(String ipServiceContent) {
        this.ipServiceContent = ipServiceContent;
    }

    public Integer getIpState() {
        return ipState;
    }

    public void setIpState(Integer ipState) {
        this.ipState = ipState;
    }

    public String getIpStateTime() {
        return ipStateTime;
    }

    public void setIpStateTime(String ipStateTime) {
        this.ipStateTime = ipStateTime;
    }

    public String getIpRemark() {
        return ipRemark;
    }

    public void setIpRemark(String ipRemark) {
        this.ipRemark = ipRemark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", ipId=").append(ipId);
        sb.append(", ipIsId=").append(ipIsId);
        sb.append(", ipPlan=").append(ipPlan);
        sb.append(", ipPrice=").append(ipPrice);
        sb.append(", ipCreatTime=").append(ipCreatTime);
        sb.append(", ipServiceContent=").append(ipServiceContent);
        sb.append(", ipState=").append(ipState);
        sb.append(", ipStateTime=").append(ipStateTime);
        sb.append(", ipRemark=").append(ipRemark);
        sb.append("]");
        return sb.toString();
    }
}