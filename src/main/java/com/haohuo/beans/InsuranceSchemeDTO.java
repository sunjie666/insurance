package com.haohuo.beans;

import java.util.List;

/**
 * Created by HaoHuo on 2019/12/4.
 */
public class InsuranceSchemeDTO extends InsuranceScheme {

    private List<InsurancePlanDTO> plan;

    private  List<InsurancePlan> planList;

    public List<InsurancePlanDTO> getPlan() {
        return plan;
    }

    public void setPlan(List<InsurancePlanDTO> plan) {
        this.plan = plan;
    }

    public List<InsurancePlan> getPlanList() {
        return planList;
    }

    public void setPlanList(List<InsurancePlan> planList) {
        this.planList = planList;
    }
}
