package com.haohuo.beans;

import java.io.Serializable;

public class Insurance implements Serializable {
    private Integer inId;

    private Integer inIpId;

    private String inInsuranceName;

    private String inInsuranceNum;

    private String inInsuranceCompany;

    private Integer inInsuranceCompanyid;

    private Integer inRiskLevel;

    private Integer inMaxPrice;

    private String inInsuranceJson;

    private Integer inConfigurationNumber;

    private String inInsuranceType;

    private Integer inInsuredNumber;

    private Integer inState;

    private String inCreateTime;

    private String inRejectedlabel;

    private String inAgreementJson;

    private String inRulesJson;

    private String inDescriptionJson;

    private String inInsuranceUrl;

    private String inInsuranceClause;

    private String inInsuranceEnclosure;

    private String inRemark;

    private static final long serialVersionUID = 1L;

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Integer getInIpId() {
        return inIpId;
    }

    public void setInIpId(Integer inIpId) {
        this.inIpId = inIpId;
    }

    public String getInInsuranceName() {
        return inInsuranceName;
    }

    public void setInInsuranceName(String inInsuranceName) {
        this.inInsuranceName = inInsuranceName;
    }

    public String getInInsuranceNum() {
        return inInsuranceNum;
    }

    public void setInInsuranceNum(String inInsuranceNum) {
        this.inInsuranceNum = inInsuranceNum;
    }

    public String getInInsuranceCompany() {
        return inInsuranceCompany;
    }

    public void setInInsuranceCompany(String inInsuranceCompany) {
        this.inInsuranceCompany = inInsuranceCompany;
    }

    public Integer getInInsuranceCompanyid() {
        return inInsuranceCompanyid;
    }

    public void setInInsuranceCompanyid(Integer inInsuranceCompanyid) {
        this.inInsuranceCompanyid = inInsuranceCompanyid;
    }

    public Integer getInRiskLevel() {
        return inRiskLevel;
    }

    public void setInRiskLevel(Integer inRiskLevel) {
        this.inRiskLevel = inRiskLevel;
    }

    public Integer getInMaxPrice() {
        return inMaxPrice;
    }

    public void setInMaxPrice(Integer inMaxPrice) {
        this.inMaxPrice = inMaxPrice;
    }

    public String getInInsuranceJson() {
        return inInsuranceJson;
    }

    public void setInInsuranceJson(String inInsuranceJson) {
        this.inInsuranceJson = inInsuranceJson;
    }

    public Integer getInConfigurationNumber() {
        return inConfigurationNumber;
    }

    public void setInConfigurationNumber(Integer inConfigurationNumber) {
        this.inConfigurationNumber = inConfigurationNumber;
    }

    public String getInInsuranceType() {
        return inInsuranceType;
    }

    public void setInInsuranceType(String inInsuranceType) {
        this.inInsuranceType = inInsuranceType;
    }

    public Integer getInInsuredNumber() {
        return inInsuredNumber;
    }

    public void setInInsuredNumber(Integer inInsuredNumber) {
        this.inInsuredNumber = inInsuredNumber;
    }

    public Integer getInState() {
        return inState;
    }

    public void setInState(Integer inState) {
        this.inState = inState;
    }

    public String getInCreateTime() {
        return inCreateTime;
    }

    public void setInCreateTime(String inCreateTime) {
        this.inCreateTime = inCreateTime;
    }

    public String getInRejectedlabel() {
        return inRejectedlabel;
    }

    public void setInRejectedlabel(String inRejectedlabel) {
        this.inRejectedlabel = inRejectedlabel;
    }

    public String getInAgreementJson() {
        return inAgreementJson;
    }

    public void setInAgreementJson(String inAgreementJson) {
        this.inAgreementJson = inAgreementJson;
    }

    public String getInRulesJson() {
        return inRulesJson;
    }

    public void setInRulesJson(String inRulesJson) {
        this.inRulesJson = inRulesJson;
    }

    public String getInDescriptionJson() {
        return inDescriptionJson;
    }

    public void setInDescriptionJson(String inDescriptionJson) {
        this.inDescriptionJson = inDescriptionJson;
    }

    public String getInInsuranceUrl() {
        return inInsuranceUrl;
    }

    public void setInInsuranceUrl(String inInsuranceUrl) {
        this.inInsuranceUrl = inInsuranceUrl;
    }

    public String getInInsuranceClause() {
        return inInsuranceClause;
    }

    public void setInInsuranceClause(String inInsuranceClause) {
        this.inInsuranceClause = inInsuranceClause;
    }

    public String getInInsuranceEnclosure() {
        return inInsuranceEnclosure;
    }

    public void setInInsuranceEnclosure(String inInsuranceEnclosure) {
        this.inInsuranceEnclosure = inInsuranceEnclosure;
    }

    public String getInRemark() {
        return inRemark;
    }

    public void setInRemark(String inRemark) {
        this.inRemark = inRemark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", inId=").append(inId);
        sb.append(", inIpId=").append(inIpId);
        sb.append(", inInsuranceName=").append(inInsuranceName);
        sb.append(", inInsuranceNum=").append(inInsuranceNum);
        sb.append(", inInsuranceCompany=").append(inInsuranceCompany);
        sb.append(", inInsuranceCompanyid=").append(inInsuranceCompanyid);
        sb.append(", inRiskLevel=").append(inRiskLevel);
        sb.append(", inMaxPrice=").append(inMaxPrice);
        sb.append(", inInsuranceJson=").append(inInsuranceJson);
        sb.append(", inConfigurationNumber=").append(inConfigurationNumber);
        sb.append(", inInsuranceType=").append(inInsuranceType);
        sb.append(", inInsuredNumber=").append(inInsuredNumber);
        sb.append(", inState=").append(inState);
        sb.append(", inCreateTime=").append(inCreateTime);
        sb.append(", inRejectedlabel=").append(inRejectedlabel);
        sb.append(", inAgreementJson=").append(inAgreementJson);
        sb.append(", inRulesJson=").append(inRulesJson);
        sb.append(", inDescriptionJson=").append(inDescriptionJson);
        sb.append(", inInsuranceUrl=").append(inInsuranceUrl);
        sb.append(", inInsuranceClause=").append(inInsuranceClause);
        sb.append(", inInsuranceEnclosure=").append(inInsuranceEnclosure);
        sb.append(", inRemark=").append(inRemark);
        sb.append("]");
        return sb.toString();
    }
}