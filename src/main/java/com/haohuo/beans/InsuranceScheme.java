package com.haohuo.beans;

import java.io.Serializable;

public class InsuranceScheme implements Serializable {
    private Integer isId;

    private String isSchemeId;

    private String isSchemeName;

    private Integer isSchemeType;

    private String isCreatTime;

    private String isContactsName;

    private String isContactsPhone;

    private Integer isOptId;

    private Integer isState;

    private String isStateTime;
    private String isRemark;

    private static final long serialVersionUID = 1L;

    public Integer getIsId() {
        return isId;
    }

    public void setIsId(Integer isId) {
        this.isId = isId;
    }

    public String getIsSchemeId() {
        return isSchemeId;
    }

    public void setIsSchemeId(String isSchemeId) {
        this.isSchemeId = isSchemeId;
    }

    public String getIsSchemeName() {
        return isSchemeName;
    }

    public void setIsSchemeName(String isSchemeName) {
        this.isSchemeName = isSchemeName;
    }

    public Integer getIsSchemeType() {
        return isSchemeType;
    }

    public void setIsSchemeType(Integer isSchemeType) {
        this.isSchemeType = isSchemeType;
    }

    public String getIsCreatTime() {
        return isCreatTime;
    }

    public void setIsCreatTime(String isCreatTime) {
        this.isCreatTime = isCreatTime;
    }

    public String getIsContactsName() {
        return isContactsName;
    }

    public void setIsContactsName(String isContactsName) {
        this.isContactsName = isContactsName;
    }

    public String getIsContactsPhone() {
        return isContactsPhone;
    }

    public void setIsContactsPhone(String isContactsPhone) {
        this.isContactsPhone = isContactsPhone;
    }

    public Integer getIsOptId() {
        return isOptId;
    }

    public void setIsOptId(Integer isOptId) {
        this.isOptId = isOptId;
    }

    public Integer getIsState() {
        return isState;
    }

    public void setIsState(Integer isState) {
        this.isState = isState;
    }

    public String getIsStateTime() {
        return isStateTime;
    }

    public void setIsStateTime(String isStateTime) {
        this.isStateTime = isStateTime;
    }

    public String getIsRemark() {
        return isRemark;
    }

    public void setIsRemark(String isRemark) {
        this.isRemark = isRemark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", isId=").append(isId);
        sb.append(", isSchemeId=").append(isSchemeId);
        sb.append(", isSchemeName=").append(isSchemeName);
        sb.append(", isSchemeType=").append(isSchemeType);
        sb.append(", isCreatTime=").append(isCreatTime);
        sb.append(", isContactsName=").append(isContactsName);
        sb.append(", isContactsPhone=").append(isContactsPhone);
        sb.append(", isOptId=").append(isOptId);
        sb.append(", isState=").append(isState);
        sb.append(", isStateTime=").append(isStateTime);
        sb.append(", isRemark=").append(isRemark);
        sb.append("]");
        return sb.toString();
    }
}