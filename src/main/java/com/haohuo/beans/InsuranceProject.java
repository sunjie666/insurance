package com.haohuo.beans;

import java.io.Serializable;

public class InsuranceProject implements Serializable {
    private Integer prId;

    private Integer prMpId;

    private Integer prPtId;

    private Integer prSiId;

    private Integer prPiId;

    private Integer prNewPrice;

    private String prCreatTime;

    private Integer prState;

    private String prStateTime;

    private String prRemarks;

    private static final long serialVersionUID = 1L;

    public Integer getPrId() {
        return prId;
    }

    public void setPrId(Integer prId) {
        this.prId = prId;
    }

    public Integer getPrMpId() {
        return prMpId;
    }

    public void setPrMpId(Integer prMpId) {
        this.prMpId = prMpId;
    }

    public Integer getPrPtId() {
        return prPtId;
    }

    public void setPrPtId(Integer prPtId) {
        this.prPtId = prPtId;
    }

    public Integer getPrSiId() {
        return prSiId;
    }

    public void setPrSiId(Integer prSiId) {
        this.prSiId = prSiId;
    }

    public Integer getPrPiId() {
        return prPiId;
    }

    public void setPrPiId(Integer prPiId) {
        this.prPiId = prPiId;
    }

    public Integer getPrNewPrice() {
        return prNewPrice;
    }

    public void setPrNewPrice(Integer prNewPrice) {
        this.prNewPrice = prNewPrice;
    }

    public String getPrCreatTime() {
        return prCreatTime;
    }

    public void setPrCreatTime(String prCreatTime) {
        this.prCreatTime = prCreatTime;
    }

    public Integer getPrState() {
        return prState;
    }

    public void setPrState(Integer prState) {
        this.prState = prState;
    }

    public String getPrStateTime() {
        return prStateTime;
    }

    public void setPrStateTime(String prStateTime) {
        this.prStateTime = prStateTime;
    }

    public String getPrRemarks() {
        return prRemarks;
    }

    public void setPrRemarks(String prRemarks) {
        this.prRemarks = prRemarks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", prId=").append(prId);
        sb.append(", prMpId=").append(prMpId);
        sb.append(", prPtId=").append(prPtId);
        sb.append(", prSiId=").append(prSiId);
        sb.append(", prPiId=").append(prPiId);
        sb.append(", prNewPrice=").append(prNewPrice);
        sb.append(", prCreatTime=").append(prCreatTime);
        sb.append(", prState=").append(prState);
        sb.append(", prStateTime=").append(prStateTime);
        sb.append(", prRemarks=").append(prRemarks);
        sb.append("]");
        return sb.toString();
    }
}