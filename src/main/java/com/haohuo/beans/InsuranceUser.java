package com.haohuo.beans;

import java.io.Serializable;

public class InsuranceUser implements Serializable {
    private Integer iuId;

    private Integer iuMpId;

    private Integer iuSiId;

    private Integer iuIpId;

    private String iuCreatTime;

    private Integer iuState;

    private String iuStateTime;

    private String iuRemarks;

    private static final long serialVersionUID = 1L;

    public Integer getIuId() {
        return iuId;
    }

    public void setIuId(Integer iuId) {
        this.iuId = iuId;
    }

    public Integer getIuMpId() {
        return iuMpId;
    }

    public void setIuMpId(Integer iuMpId) {
        this.iuMpId = iuMpId;
    }

    public Integer getIuSiId() {
        return iuSiId;
    }

    public void setIuSiId(Integer iuSiId) {
        this.iuSiId = iuSiId;
    }

    public Integer getIuIpId() {
        return iuIpId;
    }

    public void setIuIpId(Integer iuIpId) {
        this.iuIpId = iuIpId;
    }

    public String getIuCreatTime() {
        return iuCreatTime;
    }

    public void setIuCreatTime(String iuCreatTime) {
        this.iuCreatTime = iuCreatTime;
    }

    public Integer getIuState() {
        return iuState;
    }

    public void setIuState(Integer iuState) {
        this.iuState = iuState;
    }

    public String getIuStateTime() {
        return iuStateTime;
    }

    public void setIuStateTime(String iuStateTime) {
        this.iuStateTime = iuStateTime;
    }

    public String getIuRemarks() {
        return iuRemarks;
    }

    public void setIuRemarks(String iuRemarks) {
        this.iuRemarks = iuRemarks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", iuId=").append(iuId);
        sb.append(", iuMpId=").append(iuMpId);
        sb.append(", iuSiId=").append(iuSiId);
        sb.append(", iuIpId=").append(iuIpId);
        sb.append(", iuCreatTime=").append(iuCreatTime);
        sb.append(", iuState=").append(iuState);
        sb.append(", iuStateTime=").append(iuStateTime);
        sb.append(", iuRemarks=").append(iuRemarks);
        sb.append("]");
        return sb.toString();
    }
}