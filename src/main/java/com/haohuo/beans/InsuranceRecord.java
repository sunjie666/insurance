package com.haohuo.beans;

import java.io.Serializable;

public class InsuranceRecord implements Serializable {
    private Integer irId;

    private Integer irUserId;

    private Integer irOptId;

    private Integer irPtId;

    private Integer irIsId;

    private Integer irIpId;

    private Integer irState;

    private String irCreateTime;

    private String irBuyTime;

    private String irStopTime;

    private static final long serialVersionUID = 1L;

    public Integer getIrId() {
        return irId;
    }

    public void setIrId(Integer irId) {
        this.irId = irId;
    }

    public Integer getIrUserId() {
        return irUserId;
    }

    public void setIrUserId(Integer irUserId) {
        this.irUserId = irUserId;
    }

    public Integer getIrOptId() {
        return irOptId;
    }

    public void setIrOptId(Integer irOptId) {
        this.irOptId = irOptId;
    }

    public Integer getIrPtId() {
        return irPtId;
    }

    public void setIrPtId(Integer irPtId) {
        this.irPtId = irPtId;
    }

    public Integer getIrIsId() {
        return irIsId;
    }

    public void setIrIsId(Integer irIsId) {
        this.irIsId = irIsId;
    }

    public Integer getIrIpId() {
        return irIpId;
    }

    public void setIrIpId(Integer irIpId) {
        this.irIpId = irIpId;
    }

    public Integer getIrState() {
        return irState;
    }

    public void setIrState(Integer irState) {
        this.irState = irState;
    }

    public String getIrCreateTime() {
        return irCreateTime;
    }

    public void setIrCreateTime(String irCreateTime) {
        this.irCreateTime = irCreateTime;
    }

    public String getIrBuyTime() {
        return irBuyTime;
    }

    public void setIrBuyTime(String irBuyTime) {
        this.irBuyTime = irBuyTime;
    }

    public String getIrStopTime() {
        return irStopTime;
    }

    public void setIrStopTime(String irStopTime) {
        this.irStopTime = irStopTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", irId=").append(irId);
        sb.append(", irUserId=").append(irUserId);
        sb.append(", irOptId=").append(irOptId);
        sb.append(", irPtId=").append(irPtId);
        sb.append(", irIsId=").append(irIsId);
        sb.append(", irIpId=").append(irIpId);
        sb.append(", irState=").append(irState);
        sb.append(", irCreateTime=").append(irCreateTime);
        sb.append(", irBuyTime=").append(irBuyTime);
        sb.append(", irStopTime=").append(irStopTime);
        sb.append("]");
        return sb.toString();
    }
}