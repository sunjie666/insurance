package com.haohuo.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * @author fsj
 */

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ScStatusCode implements RestStatus   {
    // 使用范例 自主加 如有异常代码 直接Shift.fata(code)
    //region 系统
    RETOK(0, "正确执行"),
    RETERROR(-99, "未能正确执行"),
    NETWORKERR(99, "网络异常"),
    SYSTEMERROR(-1, "系统异常"),
    TEST_ERROE(1, "xxxx异常！"),
    CREATE_SCHEME_ERROR(5001,"创建保险方案失败"),
    CREATE_PLAN_ERROR(5002,"创建方案计划失败"),
    CREATE_INSURANCE_ERROR(5003,"创建保险失败"),
    MODIFY_SCHEME_STATE_ERROR(5004,"更新方案状态失败"),
    MODIFY_SCHEME_ERROR(5005,"更新方案信息失败"),
    MODIFY_INSURANCE_ERROR(5006,"更新保险信息失败"),
    MODIFY_PLAN_ERROR(5007,"更新计划信息失败"),
    DEL_SCHEME_REEOR(5008,"删除保障方案失败"),
    CREATE_INSURANCE_RECORD_ERROR(5009,"人员添加保障失败"),
    CREATE_RECORD_STATE_ERROR(5010,"保障方案减员失败"),
    CREATE_INSURANCE_USER_ERROR(5011,"用户配置保障方案失败"),
    NOT_EXIST_USER_INSURANCE_ERROR(5012,"当前用户暂无保障方案"),
    CREATE_INSURANCE_PROJECT_ERROR(5013,"项目配置保障方案失败"),
;
    private final int code;
    private final String message;

    /**
     * code 的构造函数
     *
     * @param code 编码
     * @return
     */
    public static ScStatusCode valueOfCode(Object code) {
        if (code != null) {
            for (ScStatusCode value : ScStatusCode.values()) {
                if (value.getCode() == (int) code) {
                    return value;
                }
            }
        }
        return null;
    }


    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
