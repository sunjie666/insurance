package com.haohuo.utils;

/**
 * 校验参数空异常
 * Created by bianzhifu on 16/8/2.
 */
public class ParmsException extends Exception {
    public ParmsException(){
        super();
    }
    public ParmsException(String msg){
        super(msg);
    }
    public ParmsException(int errorIndex){
        super("checkParms缺少第["+errorIndex+"]个必要的参数");
    }
}
