package com.haohuo.utils;

/**
 * Created by bianzhifu on 2017/4/12.
 */
public class ParmsTypeException extends Exception {
    public ParmsTypeException() {
        super();
    }

    public ParmsTypeException(String msg) {
        super(msg);
    }
}
