package com.haohuo.utils;

/**
 * Created by bianzhifu on 16/8/1.
 */
public class RetCode {
    public static int RetOK = 0;
    public static String RetOK_msg = "正确执行";
    public static int RetNullPara = 80;
    public static String RetNullPara_msg = "缺少必要的参数";
    public static int RetJsonError = 81;
    public static String RetJsonError_msg = "json格式错误";
    public static int RetParmTypeError = 82;
    public static String RetParmTypeError_msg = "参数类型错误";

}
