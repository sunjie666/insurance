package com.haohuo.utils;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by fsj on 2019/11/29.
 */
public final class Shift {

    private  Shift(){
    }
    /**
     * 返回具体的{@code RestStatus}
     *
     * @param status 自定义实体
     */
    public static ComResult usual(RestStatus status){
        checkNotNull(status);
        return ComResult.build(status);
    }

    /**
     * 抛出具体的{@code RestStatus}异常到attribute内
     *
     * @param status 自定义异常实体
     * @param details 需要拼接的变量
     */
    public static void fatal(RestStatus status, Object... details){
        checkNotNull(status);
        // 当前变量默认认为为1 若数组中有多个元素可遍历拼接到message内
        if (details.length > 0) {
            throw new RestStatusException(status.code(), status.message() + details[0]);
        }
        throw new RestStatusException(status.code(), status.message());
    }

    /**
     * 在调用服务时可能当前服务并没有对应错误码枚举
     * 调用此方法检测返回模板是否正确 若不正确则抛出RestStatusException供上层捕获
     * @param comResult
     */
    public static void checkFatal(ComResult comResult){
        checkNotNull(comResult);
        // 当前变量默认认为为1 若数组中有多个元素可遍历拼接到message内
        if (comResult.getRetCode() != 0) {
            throw new RestStatusException(comResult.getRetCode(), comResult.getRetMsg());
        }
    }

    @Deprecated
    private static void bindStatusCodesInRequestScope(String key, ErrorEntity entity) {
        checkNotNull(entity);
        checkNotNull(key);
        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            ((ServletRequestAttributes) requestAttributes).getRequest().setAttribute(key, entity);
        }
    }
}
