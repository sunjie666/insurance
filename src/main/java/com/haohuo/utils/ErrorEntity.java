package com.haohuo.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by fsj on 2019/11/29.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorEntity {

    private static final Logger log = LoggerFactory.getLogger(ErrorEntity.class);

    private static final long serialVersionUID = 2898348674857202418L;

    /**
     * 返回状态码
     */
    @JsonProperty("retCode")
    private int retCode;

    /**
     * 返回描述
     */
    @JsonProperty("retMsg")
    private String retMsg;

    /**
     * 异常信息扩展 可能不会用到
     */
    @JsonProperty("retObj")
    private Object details;

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public ErrorEntity(int retCode, String retMsg, Object details) {
        this.retCode = retCode;
        this.retMsg = retMsg;
        this.details = details;
    }

    public ErrorEntity(RestStatus restStatus){
        this.retCode = restStatus.code();
        this.retMsg = restStatus.message();
        log.debug("errorEntity: {}", this.toString());
    }

    public ErrorEntity(){
    }

    @Override
    public String toString() {
        return "ErrorEntity{" +
                "retCode=" + retCode +
                ", retMsg='" + retMsg + '\'' +
                ", details='" + details + '\'' +
                '}';
    }
}
