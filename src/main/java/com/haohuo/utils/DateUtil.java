package com.haohuo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by bianzhifu on 16/8/3.
 */
public class DateUtil {

    public final static String FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public final static String FORMAT_DEFAULT_yyyyMMddHHmmss = "yyyyMMddHHmmss";


    public static int getUnixTimeNow() {
        int unixTime = (int) (System.currentTimeMillis() / 1000);
        return unixTime;
    }

    public static int getUnixTimeToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return (int) (cal.getTimeInMillis()/1000);
    }

    public static String getToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String get_Today() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String get_Yestaday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return simpleDateFormat.format(calendar.getTime());
    }
    public static String get_Tomorrow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return simpleDateFormat.format(calendar.getTime());
    }
    public static String get_NowMonth() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }
    public static String get_NextMonth() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH,1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_NextYearToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,1);
        return simpleDateFormat.format(calendar.getTime());
    }
    public static String get_NextMonthToday() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH,1);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_SomeDay(int days) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,days);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Date getNowDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Calendar getNowCalendar() {
        Calendar calendar = Calendar.getInstance();
        return calendar;
    }

    /**
     * 获取当前时间
     *
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String getNowTimeStr() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static String getNowTimeStr(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }


    /**
     * 格式化时间 format yyyy-MM-dd HH:mm:ss
     */
    public static String formatDate(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String formatNowDate(String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(Calendar.getInstance().getTime());
    }

    public static Date formatDate(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar parseCalandar(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            Date temp = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(temp);
            return calendar;
        } catch (ParseException e) {
        }
        return null;
    }

    public static String formatCalandar(Calendar date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date.getTime());
    }

    // 取今天的前后几天
    public static String getOffsetDate(int int2) {
        Calendar rightNow = Calendar.getInstance();
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
        rightNow.add(Calendar.DAY_OF_MONTH, int2);
        // 进行时间转换
        String date = sim.format(rightNow.getTime());
        return date;
    }

    /**
     * [获取两个日期之间的天数差]<br>
     *
     * @param beginDate 开始日
     * @param endDate   结束日
     * @return 天数差
     * @throws ParseException
     * @throws Exception      异常
     */
    public static int daysBetween(String beginDate, String endDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate.replace('/', '-')));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(endDate.replace('/', '-')));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * [根据日期和天数间隔获取对应日期]<br>
     *
     * @param beginDate 开始日
     * @param days      间隔天数
     * @return 日期
     * @throws Exception 异常
     */
    public static String getDate(String beginDate, int days) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(beginDate.replace('/', '-')));
        long time1 = cal.getTimeInMillis();
        long time2 = time1 + (long) days * 1000 * 3600 * 24;
        // 进行时间转换
        String date = sdf.format(new Date(time2));
        return date;
    }

    // 根据时间段，取其每周一的集合
    public static List<String> dealDiffDateWeek(String minDate, String maxDate) throws ParseException {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 格式化为年月日
        Calendar min = Calendar.getInstance();
        min.setTime(sdf.parse(minDate));

        //判断起始日期为周一也增加到集合中
        if ((min.get(Calendar.DAY_OF_WEEK)) == 2) {
            result.add(sdf.format(min.getTime()));
        }

        //取时间段中的其他周一
        for (int i = 0; i < daysBetween(minDate, maxDate); i++) {
            min.add(Calendar.DAY_OF_MONTH, 1);
            if ((min.get(Calendar.DAY_OF_WEEK)) == 2) {
                result.add(sdf.format(min.getTime()));
            }
            if (maxDate.equals(sdf.format(min.getTime())))
                break;
        }
        return result;
    }

    public static List<String> getMonthBetween(String minDate, String maxDate) throws ParseException {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");// 格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(sdf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(sdf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    //日期转化为秒
    public static int getTimestamp(String datestr, String sf) {
        int unixTime = (int) (formatDate(datestr, sf).getTime() / 1000);
        if (unixTime < 0) {
            unixTime = Integer.MAX_VALUE;
        }
        return unixTime;
    }

    //秒转化为日期
    public static String getTimestampToDate(int datestr, String sf) {

        long millisecond = datestr * 1000l;

        return formatDate(new Date(millisecond), sf);
    }

    public static String get_SomeMonth(int month) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH,month);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String get_SomeDayForMinute(int minute) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,minute);
        return simpleDateFormat.format(calendar.getTime());
    }
}
