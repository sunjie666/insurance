package com.haohuo.utils;

import java.io.Serializable;

/**
 * Created by HaoHuo on 2019/11/29.
 */
public class RestStatusException  extends RuntimeException implements Serializable {

    private static final long serialVersionUID = -6691853364250179598L;

    private Integer errorCode;
    private String message;

    /**
     * 有参构造函数
     * @param message
     */
    public RestStatusException(String message) {
        super(message);
        this.message = message;
    }

    /**
     * 有参构造函数
     * @param errorCode
     * @param message
     */
    public RestStatusException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    /**
     * 有参构造函数
     * @param message
     * @param cause
     */
    public RestStatusException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 有参数构造函数
     * @param cause
     */
    public RestStatusException(Throwable cause) {
        super(cause);
    }

    protected RestStatusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
