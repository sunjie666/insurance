package com.haohuo.compose;


import com.haohuo.utils.ComResult;
import com.haohuo.utils.RetCode;
import com.haohuo.utils.ScStatusCode;

/**
 * Created by yaochunyu on 2018/7/7.
 */
public class BaseCompose {
    protected ComResult error(int code, String msg, Object obj) {
        return new ComResult(false, code, msg, obj);
    }

    protected ComResult error(int code, String msg) {
        return new ComResult(false, code, msg, null);
    }

    protected ComResult success() {
        return success(null);
    }

    protected ComResult success(Object obj) {
        return success(RetCode.RetOK_msg, obj);
    }

    protected ComResult success(String msg, Object obj) {
        return success(RetCode.RetOK, msg, obj);
    }

    protected ComResult success(int code, String msg, Object obj) {
        return new ComResult(true, code, msg, obj);
    }
}
