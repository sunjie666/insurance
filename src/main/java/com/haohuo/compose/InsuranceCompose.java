package com.haohuo.compose;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.haohuo.beans.*;
import com.haohuo.service.InsuranceService;
import com.haohuo.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 * Created by HaoHuo on 2019/12/3.
 */
@Slf4j
@Service
public class InsuranceCompose extends BaseCompose {

    @Autowired
    private InsuranceService insuranceService;


    /**创建保险
     *
     * @param obj
     * @return
     */
    @Transactional
    public ComResult createInsurance(JSONObject obj) {
        Insurance insurance = new Insurance();
        InsurancePlan plan = new InsurancePlan();
        InsuranceScheme scheme = new InsuranceScheme();
        //创建保险方案
        scheme.setIsSchemeId(obj.getString("schemeId"));
        scheme.setIsSchemeName(obj.getString("schemeName"));
        scheme.setIsSchemeType(obj.getInteger("schemeType"));
        scheme.setIsContactsName(obj.getString("contactName"));
        scheme.setIsContactsPhone(obj.getString("contactPhone"));
        scheme.setIsOptId(obj.getInteger("optId"));
        scheme.setIsState(1);
        scheme.setIsCreatTime(DateUtil.getNowTimeStr());
        if (insuranceService.createScheme(scheme)<0){
            Shift.usual(ScStatusCode.CREATE_SCHEME_ERROR);
        }
        //一个方案对应多个计划
        JSONArray planArray = obj.getJSONArray("plan");
            for (int n = 0; n < planArray.size(); n++) {
                plan.setIpPlan(planArray.getJSONObject(n).getString("planName"));
                plan.setIpPrice(planArray.getJSONObject(n).getInteger("planPrice"));
                plan.setIpIsId(scheme.getIsId());
                plan.setIpState(1);
                plan.setIpCreatTime(DateUtil.getNowTimeStr());
                plan.setIpServiceContent(planArray.getJSONObject(n).getString("serviceJson"));
                //创建方案
                if (insuranceService.createPlan(plan) < 0) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return Shift.usual(ScStatusCode.CREATE_PLAN_ERROR);
                }

                //一个计划对应多个保险
                insurance.setInIpId(plan.getIpId());
                JSONArray insuranceList = planArray.getJSONObject(n).getJSONArray("insuranceList");
                for (int j = 0; j < insuranceList.size(); j++) {
                    insurance.setInInsuranceName(insuranceList.getJSONObject(j).getString("insuranceName"));
                    insurance.setInInsuranceNum(insuranceList.getJSONObject(j).getString("insuranceNum"));
                    insurance.setInRiskLevel(insuranceList.getJSONObject(j).getInteger("riskLevel"));
                    insurance.setInInsuranceCompany(insuranceList.getJSONObject(j).getString("insuranceCompy"));
                    insurance.setInRejectedlabel(insuranceList.getJSONObject(j).getString("rejectedlabel"));
                    insurance.setInMaxPrice(insuranceList.getJSONObject(j).getInteger("insuranceMaxPrice"));
                    insurance.setInAgreementJson(insuranceList.getJSONObject(j).getString("agreementJson"));
                    insurance.setInRulesJson(insuranceList.getJSONObject(j).getString("rulesJson"));
                    insurance.setInDescriptionJson(insuranceList.getJSONObject(j).getString("descriptionJson"));
                    insurance.setInInsuranceUrl(insuranceList.getJSONObject(j).getString("insuranceUrl"));
                    insurance.setInInsuranceClause(insuranceList.getJSONObject(j).getString("insuranceClause"));
                    insurance.setInInsuranceEnclosure(insuranceList.getJSONObject(j).getString("insuranceEnclosure"));
                    insurance.setInInsuranceJson(insuranceList.getJSONObject(j).getString("insuranceJson"));
                    insurance.setInState(1);
                    insurance.setInCreateTime(DateUtil.getNowTimeStr());
                    //创建保险
                    if (insuranceService.createInsurance(insurance) < 0) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        return Shift.usual(ScStatusCode.CREATE_INSURANCE_ERROR);
                    }
                }
            }
        return  success();
    }

    /**查询方案列表
     *
     * @param obj
     * @return
     */
    public ComResult getSchemeListByWhere(JSONObject obj) {
        Map<String,Object>map = new HashMap<>();
        if (StringUtils.isNotEmpty(obj.getString("schemeName"))){
            map.put("schemeName",obj.getString("schemeName"));
        }
        if (obj.getInteger("state")!=null && obj.getInteger("state")>0){
            map.put("state",obj.getInteger("state"));
        }
        if (obj.getInteger("index") != null && obj.getInteger("count") != null && obj.getInteger("count") > 0) {
            map.put("start", obj.getInteger("index") * obj.getInteger("count"));
            map.put("count", obj.getInteger("count"));
        }
        JSONObject result = new JSONObject();
        List<BeanMap> list = insuranceService.getSchemeListByWhere(map);
        if (list != null && list.size() > 0) {
            result.put("data", list);
            if (obj.getInteger("index") != null && obj.getInteger("count") != null
                    && obj.getInteger("count") > 0) {
                result.put("count", insuranceService.getSchemeListCount(map));
            } else {
                result.put("count", list.size());
            }
        } else {
            result.put("data", new ArrayList<>());
            result.put("count", 0);
        }
        return success(result);
    }

    /**更新方案状态
     *
     * @param obj
     * @return
     */
    public ComResult modifySchemeState(JSONObject obj) {
        InsuranceScheme scheme = new InsuranceScheme();
        scheme.setIsId(obj.getInteger("isId"));
        scheme.setIsState(obj.getInteger("state"));
        scheme.setIsStateTime(DateUtil.getNowTimeStr());
        if (insuranceService.modifyScheme(scheme)<0){
            return  Shift.usual(ScStatusCode.MODIFY_SCHEME_STATE_ERROR);
        }
        return success();
    }

    /**查询方案详情
     *
     * @param obj
     * @return
     */
    public ComResult getSchemeInfoById(JSONObject obj) {
        InsuranceSchemeDTO schemeDTO= new InsuranceSchemeDTO();
        InsurancePlanDTO planDTO = new InsurancePlanDTO();
        List<InsurancePlanDTO>planDTOList = new ArrayList<>();
        List<Insurance>insuranceList= new ArrayList<>();
        //获取方案信息
        InsuranceScheme scheme  =insuranceService.getSchemeInfoById(obj.getInteger("isId"));
        //方案copy->DTO
        BeanUtils.copyProperties(scheme,schemeDTO);
        //获取计划信息
        List<InsurancePlan>planList  = insuranceService.getPlanBySchemeId(obj.getInteger("isId"));
        //获取保险信息
        for (InsurancePlan insurancePlan:planList){
            BeanUtils.copyProperties(insurancePlan,planDTO);
            insuranceList = insuranceService.getInsuranceByPlanId(insurancePlan.getIpId());
            planDTO.setInsuranceList(insuranceList);
            planDTOList.add(planDTO);
        }
        schemeDTO.setPlan(planDTOList);
        return  success(schemeDTO);
    }

    /**更新方案详情
     *
     * @param obj
     * @return
     */
    @Transactional
    public ComResult modifySchemeInfo(JSONObject obj) {
        InsuranceScheme scheme = new InsuranceScheme();
        InsurancePlan plan = new InsurancePlan();
        Insurance insurance = new Insurance();

        if (StringUtils.isNotEmpty(obj.getString("contactName"))||StringUtils.isNotEmpty(obj.getString("contactPhone"))){
            scheme.setIsId(obj.getInteger("isId"));
            scheme.setIsContactsName(obj.getString("contactName"));
            scheme.setIsContactsPhone(obj.getString("contactPhone"));
            if (insuranceService.modifyScheme(scheme)<0){
                return Shift.usual(ScStatusCode.MODIFY_SCHEME_ERROR);
            }
        }
        if (StringUtils.isNotEmpty(obj.getString("insuranceNum"))||StringUtils.isNotEmpty(obj.getString("insuranceCompy"))||
                obj.getInteger("riskLevel")>0||StringUtils.isNotEmpty(obj.getString("rejectedlabel"))||
                StringUtils.isNotEmpty(obj.getString("agreementJson"))||StringUtils.isNotEmpty(obj.getString("rulesJson"))||
                StringUtils.isNotEmpty(obj.getString("descriptionJson"))){

            insurance.setInId(obj.getInteger("inId"));
            insurance.setInInsuranceNum(obj.getString("insuranceNum"));
            insurance.setInInsuranceCompany(obj.getString("insuranceCompy"));
            insurance.setInRiskLevel(obj.getInteger("riskLevel"));
            insurance.setInRejectedlabel(obj.getString("rejectedlabel"));
            insurance.setInAgreementJson(obj.getString("agreementJson"));
            insurance.setInRulesJson(obj.getString("rulesJson"));
            insurance.setInDescriptionJson(obj.getString("descriptionJson"));
            if (insuranceService.modifyInsurance(insurance)<0){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return Shift.usual(ScStatusCode.MODIFY_INSURANCE_ERROR);
            }
        }

        if (StringUtils.isNotEmpty(obj.getString("serviceJson"))){
            plan.setIpId(obj.getInteger("ipId"));
            plan.setIpServiceContent(obj.getString("serviceJson"));
            if (insuranceService.modifyPlan(plan)<0){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return Shift.usual(ScStatusCode.MODIFY_PLAN_ERROR);
            }
        }

        return success();
    }

    //删除方案
    public ComResult delSchemeById(JSONObject obj) {
        //删除方案和方案计划还有具体的保险
       if (insuranceService.delSChemeById(obj.getInteger("isId"))<0){
           return Shift.usual(ScStatusCode.DEL_SCHEME_REEOR);
       }
        return success();
    }

    /**获取企业列表和方案列表
     *
     * @param obj
     * @return
     */
    public ComResult getOptInfoAndSchemeInfo(JSONObject obj) {
        JSONObject reusult=new JSONObject();
        if (StringUtils.isNotEmpty(obj.getString("userName"))){
          List<BeanMap>userList = insuranceService.getUserListByUserName(obj.getString("userName"));
            if (userList.size()>0){
                reusult.put("userList",userList);
            }else {
                reusult.put("userList",new ArrayList<>());
            }
        }
        if (StringUtils.isNotEmpty(obj.getString("schemeName"))){
            List<InsuranceScheme> schemeList =insuranceService.getSchemeListBySchemeName(obj.getString("schemeName"));
           if (schemeList.size()>0){
               reusult.put("schemeList",schemeList);
           }else {
               reusult.put("schemeList",new ArrayList<>());
           }
        }
        return  success(reusult);
    }

    /**获取计划列表
     *
     * @param obj
     * @return
     */
    public ComResult getPlanListByIsId(JSONObject obj) {
        List<InsurancePlan> planList = insuranceService.getPlanByIsId(obj.getInteger("isId"));
        if (planList.size()>0){
            return success(planList);
        }else {
            return success(new ArrayList<>());
        }
    }

    /**获取增员人员名单列表
     *
     * @param obj
     * @return
     */
    public ComResult getOptBykeyWords(JSONObject obj) {
        JSONObject reusult=new JSONObject();
        Map<String,Object>optMap = new HashMap<>();
        if (obj.getInteger("mpId")!=null){
            optMap.put("userId",obj.getInteger("mpId"));
        }else {
            optMap.put("userId",0);
        }
        optMap.put("irType",obj.getInteger("irType"));
        optMap.put("keyWords",obj.getString("optKeyWords"));
        List<BeanMap>optList=insuranceService.getOptByKeyWords(optMap);
        if (optList.size()>0){
            reusult.put("optList",optList);
        }else {
            reusult.put("optList",new ArrayList<>());
        }
        return  success(reusult);
    }

    /**增员
     *
     * @param obj
     * @return
     */
    public ComResult createBuyPlanRecord(JSONObject obj) {
        InsuranceRecord record = new InsuranceRecord();
        JSONArray optList = obj.getJSONArray("optList");
        for (int i =0;i<optList.size();i++ ){
            record.setIrCreateTime(DateUtil.getNowTimeStr());
            record.setIrBuyTime(DateUtil.getNowTimeStr());
            record.setIrState(1);
            record.setIrUserId(obj.getInteger("mpId"));
            record.setIrPtId(optList.getJSONObject(i).getInteger("ptId"));
            record.setIrIsId(optList.getJSONObject(i).getInteger("isId"));
            record.setIrIpId(optList.getJSONObject(i).getInteger("ipId"));
            record.setIrOptId(optList.getJSONObject(i).getInteger("optId"));
            if (insuranceService.createRecord(record)<0){
                return Shift.usual(ScStatusCode.CREATE_INSURANCE_RECORD_ERROR);
            }
        }
        return success();
    }

    /**减员
     *
     * @param obj
     * @return
     */
    public ComResult cutBuyPlanRecord(JSONObject obj) {
       Map<String,Object>recordMap = new HashMap<>();
        recordMap.put("stopTime",DateUtil.getNowTimeStr());
        recordMap.put("userId",obj.getInteger("mpId"));
        recordMap.put("optIds",obj.getString("optIds"));
        if (insuranceService.cutBuyPlanRecord(recordMap)<0){
            return Shift.usual(ScStatusCode.CREATE_RECORD_STATE_ERROR);
        }
        return success();
    }

    /**获取用户需要配置的方案列表和对应计划信息
     *
     * @param obj
     * @return
     */
    public ComResult getSchemeAndPlan(JSONObject obj) {
        List<Object>list = new ArrayList<>();
        Map<String,Object>map = new HashMap<>();
        map.put("state",1);
        List<BeanMap> schemeList = insuranceService.getSchemeListByWhere(map);
        for(BeanMap schemeMap:schemeList){
            InsuranceSchemeDTO schemeDTO = new InsuranceSchemeDTO();
            schemeDTO.setIsId(schemeMap.getIntegerValue("isId"));
            schemeDTO.setIsSchemeName(schemeMap.getStringValue("IsSchemeName"));
            List<InsurancePlan> planList = insuranceService.getPlanBySchemeId(schemeMap.getIntegerValue("isId"));
            schemeDTO.setPlanList(planList);
            list.add(schemeDTO);
        }
        return  success(list);
    }

    /**用户配置保障方案
     *
     * @param obj
     * @return
     */
    public ComResult userAddInsurance(JSONObject obj) {
        JSONArray schemeList = obj.getJSONArray("schemeJson");
        for(int i =0;i<schemeList.size();i++){
            String[] planList = schemeList.getJSONObject(i).getString("ipIds").split(",");
            for(String planId:planList){
                InsuranceUser insuranceUser = new InsuranceUser();
                insuranceUser.setIuMpId(obj.getInteger("mpId"));
                insuranceUser.setIuSiId(schemeList.getJSONObject(i).getInteger("isId"));
                insuranceUser.setIuIpId(Integer.parseInt(planId));
                insuranceUser.setIuCreatTime(DateUtil.getNowTimeStr());
                insuranceUser.setIuState(1);
                if (insuranceService.createInsuranceUser(insuranceUser)<0){
                    return Shift.usual(ScStatusCode.CREATE_INSURANCE_USER_ERROR);
                }
            }
        }
        return success();
    }

    /**获取项目配置需要的方案和计划
     *
     * @param obj
     * @return
     */
    public ComResult getProjectSchemeAndPlan(JSONObject obj) {
      List<BeanMap>insuranceUserList =  insuranceService.getUserInsuranceByUserId(obj.getInteger("mpId"));
      if (insuranceUserList.size()<0){
          return Shift.usual(ScStatusCode.NOT_EXIST_USER_INSURANCE_ERROR);
      }
      return success(insuranceUserList);
    }


    /**项目配置方案
     *
     * @param obj
     * @return
     */
    public ComResult projectAddInsurance(JSONObject obj) {
        InsuranceProject insuranceProject = new InsuranceProject();
        insuranceProject.setPrCreatTime(DateUtil.getNowTimeStr());
        insuranceProject.setPrMpId(obj.getInteger("mpId"));
        insuranceProject.setPrPtId(obj.getInteger("ptId"));
        insuranceProject.setPrSiId(obj.getInteger("siId"));
        insuranceProject.setPrPiId(obj.getInteger("piId"));
        insuranceProject.setPrNewPrice(obj.getInteger("newPrice"));
        insuranceProject.setPrState(1);
        if (insuranceService.createInsuranceProject(insuranceProject)<0){
            return Shift.usual(ScStatusCode.CREATE_INSURANCE_PROJECT_ERROR);
        }
        return success();
    }
}
