package com.haohuo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.haohuo.mapper"})
public class SecurityCenterApplication {
	public static void main(String[] args) {
		SpringApplication.run(SecurityCenterApplication.class, args);
	}

}
