package com.haohuo.controller;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.compose.InsuranceCompose;
import com.haohuo.utils.ParmsException;
import com.haohuo.utils.ScStatusCode;
import com.haohuo.utils.Shift;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fsj
 * @date 2019/11/29
 */


@RestController
@Api(tags = "SecurityCenter", description = "保障中心")
@RequestMapping(value = {"/API/BusIns"})
public class BusinessInsuranceController extends BaseController{

    @Autowired
    private InsuranceCompose insuranceCompose;

    @RequestMapping(value = "createInsurance", method = RequestMethod.POST)
    @ApiOperation(value = " 添加保险", notes = "" +
            "######输入：\n" +
            "\"{\n" +
            "\"schemeId\":\"方案自定义ID Y\",\n" +
            "\"schemeName\":\"方案名称 Y\",\n" +
            "\"schemeType\":\"方案类型：1骑手 2非骑手 Y\",\n" +
            "\"contactName\":\"联系人姓名 Y\",\n" +
            "\"contactPhone\":\"联系人电话 Y\",\n" +
            "\"optId\":\"创建人id Y\",\n" +
            "\"plan\":[\n" +
            "{\n" +
            "\"planName\":\"计划名称1\",\n" +
            "\"planPrice\":10,\n" +
            "\"insuranceList\":[\n" +
            "{\n" +
            "\"insuranceName\":\"保险名称1\",\n" +
            "\"insuranceNum\":\"保险单号1\",\n" +
            "\"insuranceJson\":[{\"insuranceContent\":\"保险内容1\",\"insurancePrice\":1000},{\"insuranceContent\":\"保险内容2\",\"insurancePrice\":2000}],\n" +
            "\"insuranceCompy\":\"保险公司1\",\n" +
            "\"riskLevel\":\"风险等级:1低风险2中风险3高风险4特级风险 Y\",\n" +
            "\"rejectedlabel\":\"多个逗号分隔，拒保标签手写输入\",\n" +
            "\"insuranceMaxPrice\":最大保额,\n" +
            "\"agreementJson\":\"特殊约定1\",\n" +
            "\"rulesJson\":\"理赔规则1\",\n" +
            "\"descriptionJson\":\"报案说明1\",\n" +
            "\"insuranceUrl\":\"保险协议路径1\",\n" +
            "\"insuranceClause\":\"保险条款路径11\",\n" +
            "\"insuranceEnclosure\":\"保险附件1 N\"\n" +
            "},\n" +
            "{\"insuranceName\":\"保险名称2\",\n" +
            "\"insuranceNum\":\"保险单号2\",\n" +
            "\"insuranceJson\":[{\"insuranceContent\":\"保险内容2\",\"insurancePrice\":12000},{\"insuranceContent\":\"保险内容2\",\"insurancePrice\":22000}],\n" +
            "\"insuranceCompy\":\"保险公司2222\",\n" +
            "\"riskLevel\":22,\n" +
            "\"rejectedlabel\":\"\",\n" +
            "\"insuranceMaxPrice\":22000,\n" +
            "\"agreementJson\":\"特殊约定2\",\n" +
            "\"rulesJson\":\"理赔规则2\",\n" +
            "\"descriptionJson\":\"报案说明2\",\n" +
            "\"insuranceUrl\":\"保险协议路径2\",\n" +
            "\"insuranceClause\":\"保险条款路径22222\",\n" +
            "\"insuranceEnclosure\":\"保险附件22 N\"}],\n" +
            "\"serviceJson\":[{\"serviceContent2\":\"计划服务内容2\"},{\"serviceContent2\":\"计划服务内容2\"}]\n" +
            "}\n" +
            "]\n" +
            "}" +
            "######输出：成功/失败\n" +
            "")
    Object createInsurance(@RequestBody JSONObject obj) throws ParmsException {
        checkParms(obj,"schemeId","schemeName","schemeType","contactName",
                "contactPhone","optId","plan"
        );
        return insuranceCompose.createInsurance(obj);
    }



    @RequestMapping(value = "getSchemeListByWhere", method = RequestMethod.POST)
    @ApiOperation(value = "查询保障方案列表", notes = "" +
            "输入：\n" +
            "{" +
            "\"schemeName\":\"方案名称 N\",\n" +
            "\"state\":\"方案状态:1启用 2停用 N\",\n" +
            "\"index\": 分页页码 从0开始  不传不分页 N,\n" +
            "\"count\": 每页条数 不传/传0不分页 N\n" +
            "}\n" +
            "输出：方案列表\n" +
            "")
    Object getSchemeListByWhere(@RequestBody JSONObject obj) {
        return insuranceCompose.getSchemeListByWhere(obj);
    }


    @RequestMapping(value = "modifySchemeState", method = RequestMethod.POST)
    @ApiOperation(value = "更新方案状态", notes = "" +
            "输入：\n" +
            "{" +
            "\"isId\":\"方案id Y\",\n" +
            "\"state\":\"方案状态:1启用 2停用 Y\",\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object modifySchemeState(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"isId","state");
        return insuranceCompose.modifySchemeState(obj);
    }


    @RequestMapping(value = "getSchemeInfoById", method = RequestMethod.POST)
    @ApiOperation(value = "查询方案详情", notes = "" +
            "输入：\n" +
            "{" +
            "\"isId\":\"方案id Y\",\n" +
            "}\n" +
            "输出：方案详情\n" +
            "")
    Object getSchemeInfoById(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"isId");
        return insuranceCompose.getSchemeInfoById(obj);
    }


    @RequestMapping(value = "modifySchemeInfo", method = RequestMethod.POST)
    @ApiOperation(value = "更新方案信息", notes = "" +
            "输入：\n" +
            "{" +
            "\"isId\":\"方案id:如果修改了联系人姓名/联系人电话必传 Y\",\n" +
            "\"contactName\":\"联系人姓名 N\",\n" +
            "\"contactPhone\":\"联系人电话 N\",\n" +
            "\"inId\":\"保险id：如果修改了保险相关字段必传 N\",\n" +
            "\"insuranceNum\":\"保险单号\",\n" +
            "\"insuranceCompy\":\"保险公司1\",\n" +
            "\"riskLevel\":\"风险等级:1低风险2中风险3高风险4特级风险 Y\",\n" +
            "\"rejectedlabel\":\"多个逗号分隔，拒保标签手写输入\",\n" +
            "\"agreementJson\":\"特殊约定\",\n" +
            "\"rulesJson\":\"理赔规则\",\n" +
            "\"descriptionJson\":\"报案说明\",\n" +
            "\"ipId\":\"计划id:如果修改服务内容必传N\",\n" +
            "\"serviceJson\":[{\"serviceContent2\":\"计划服务内容2\"},{\"serviceContent2\":\"计划服务内容2\"}]\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object modifySchemeInfo(@RequestBody JSONObject obj)throws ParmsException {
        if (StringUtils.isNotEmpty(obj.getString("contactName"))||StringUtils.isNotEmpty(obj.getString("contactPhone"))){
            checkParms(obj.getInteger("isId"));
        }
        if (StringUtils.isNotEmpty(obj.getString("insuranceNum"))||StringUtils.isNotEmpty(obj.getString("insuranceCompy"))||
                obj.getInteger("riskLevel")>0||StringUtils.isNotEmpty(obj.getString("rejectedlabel"))||
                StringUtils.isNotEmpty(obj.getString("agreementJson"))||StringUtils.isNotEmpty(obj.getString("rulesJson"))||
                StringUtils.isNotEmpty(obj.getString("descriptionJson"))){
            checkParms(obj.getInteger("inId"));
        }
        if (StringUtils.isNotEmpty(obj.getString("serviceJson"))){
            checkParms(obj.getInteger("ipId"));
        }
        return insuranceCompose.modifySchemeInfo(obj);
    }


    @RequestMapping(value = "delSchemeById", method = RequestMethod.POST)
    @ApiOperation(value = "删除方案", notes = "" +
            "输入：\n" +
            "{" +
            "\"isId\":\"方案id Y\"\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object delSchemeById(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"isId");
        return insuranceCompose.delSchemeById(obj);
    }


    @RequestMapping(value = "getSchemeInfo", method = RequestMethod.POST)
    @ApiOperation(value = "查询增员方案信息", notes = "" +
            "输入：\n" +
            "{" +
            "\"userName\":\"企业名称 Y\",\n" +
            "\"schemeName\":\"方案名称 Y\"\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object getOptInfoAndSchemeInfo(@RequestBody JSONObject obj)throws ParmsException {
        return insuranceCompose.getOptInfoAndSchemeInfo(obj);
    }

    @RequestMapping(value = "getOptBykeyWords", method = RequestMethod.POST)
    @ApiOperation(value = "查询增/减员人员信息", notes = "" +
            "输入：\n" +
            "{" +
            "\"mpId\":\"用户id Y\",\n" +
            "\"irType\":\"投保状态：0未投保 1投保 2 停保 Y\",\n" +
            "\"optKeyWords\":\"师傅姓名/师傅身份证 N\"\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object getOptBykeyWords(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId","irType","optKeyWords");
        return insuranceCompose.getOptBykeyWords(obj);
    }



    @RequestMapping(value = "getPlanListByIsId", method = RequestMethod.POST)
    @ApiOperation(value = "根据方案id查询方案计划", notes = "" +
            "输入：\n" +
            "{" +
            "\"isId\":\"方案id Y\"\n" +
            "}\n" +
            "输出：成功/失败\n" +
            "")
    Object getPlanListByIsId(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"isId");
        return insuranceCompose.getPlanListByIsId(obj);
    }


    @RequestMapping(value = "createBuyPlanRecord", method = RequestMethod.POST)
    @ApiOperation(value = "保障增员", notes = "" +
            "输入：\n" +
            "{\n" +
            "\"mpId\":\"用户id Y\",\n" +
            "\"optList\":[\n" +
            "{\"optId\":\"师傅optid Y\",\"ptId\":\"项目id Y\",\"isId\":\"方案id Y\",\"ipId\":\"计划id Y\"}," +
            "{\"optId\":1,\"ptId\":\"项目id Y\",\"isId\":\"方案id Y\",\"ipId\":\"计划id Y\"}\n" +
            "]\n" +
            "}" +
            "输出：成功/失败\n" +
            "")
    Object createBuyPlanRecord(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId","optList");
        return insuranceCompose.createBuyPlanRecord(obj);
    }


    @RequestMapping(value = "cutBuyPlanRecord", method = RequestMethod.POST)
    @ApiOperation(value = "保障减员", notes = "" +
            "输入：\n" +
            "{\n" +
            "\"mpId\":\"用户id Y\",\n" +
            "\"optIds\":\"师傅id 多个逗号分隔 Y\"\n" +
            "}" +
            "输出：成功/失败\n" +
            "")
    Object cutBuyPlanRecord(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId","optIds");
        return insuranceCompose.cutBuyPlanRecord(obj);
    }


    @RequestMapping(value = "getUserSchemeAndPlan", method = RequestMethod.POST)
    @ApiOperation(value = "获取用户配置需要的方案列表和计划列表", notes = "" +
            "输入：\n" +
            "{\n" +
            "}" +
            "输出：成功/失败\n" +
            "")
    Object getSchemeAndPlan(@RequestBody JSONObject obj)throws ParmsException {
        return insuranceCompose.getSchemeAndPlan(obj);
    }


    @RequestMapping(value = "userAddInsurance", method = RequestMethod.POST)
    @ApiOperation(value = "用户配置保障方案和计划", notes = "" +
            "输入：\n" +
            "{\n" +
            "\"mpId\":\"用户id Y\",\n" +
            "\"schemeJson\":[\n" +
            "{\"isId\":\"方案id 1\",\"ipIds\":\"多个逗号分隔 \"}\n" +
            "]\n" +
            "}" +
            "输出：成功/失败\n" +
            "")
    Object userAddInsurance(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId","schemeJson");
        return insuranceCompose.userAddInsurance(obj);
    }



    @RequestMapping(value = "getProjectSchemeAndPlan", method = RequestMethod.POST)
    @ApiOperation(value = "获取项目配置需要的方案和计划", notes = "" +
            "输入：\n" +
            "{\n" +
            "\"mpId\":\"用户id Y\"\n" +
            "}" +
            "输出：列表\n" +
            "")
    Object getProjectSchemeAndPlan(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId");
        return insuranceCompose.getProjectSchemeAndPlan(obj);
    }



    @RequestMapping(value = "projectAddInsurance", method = RequestMethod.POST)
    @ApiOperation(value = "项目配置保障方案和计划", notes = "" +
            "输入：\n" +
            "{\n" +
            "\"mpId\":\"用户id Y\",\n" +
            "\"ptId\":\"项目id Y\",\n" +
            "\"siId\":\"方案id Y\",\n" +
            "\"piId\":\"计划id Y\",\n" +
            "\"newPrice\":\"新价格 Y\"\n" +
            "}" +
            "输出：成功/失败\n" +
            "")
    Object projectAddInsurance(@RequestBody JSONObject obj)throws ParmsException {
        checkParms(obj,"mpId","ptId","siId","piId","newPrice");
        return insuranceCompose.projectAddInsurance(obj);
    }
}
