package com.haohuo.controller;

import com.alibaba.fastjson.JSONObject;
import com.haohuo.utils.ComResult;
import com.haohuo.utils.ParmsException;
import com.haohuo.utils.ParmsTypeException;
import com.haohuo.utils.RetCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by bianzhifu on 16/7/26.
 */
public class BaseController {

    private static final Logger log = LoggerFactory.getLogger(BaseController.class);

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    protected int getApiCloudAppId() {
        Object appId = request.getAttribute("appId");
        try {
            return (int) appId;
        } catch (Exception e) {
            //默认的appId
            return 10000001;
        }
    }

    protected String getHHVersion() {
        try {
            return request.getHeader("hh_version");
        } catch (Exception e) {
        }
        return "";
    }

    protected boolean getHHIsAndroid() {
        try {
            if("Android".equals(request.getHeader("hh_os"))){
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
    protected boolean getHHIsIos() {
        try {
            if("iOS".equals(request.getHeader("hh_os"))){
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    protected Integer getJsonInterger(JSONObject json, String col) throws ParmsTypeException {
        try {
            return json.getInteger(col);
        } catch (Exception e) {
            throw new ParmsTypeException("参数:" + col + ",值:" + json.get(col));
        }
    }


    protected boolean checkParms(JSONObject json, String... cols) throws ParmsException {
        for (String col : cols) {
            Object o = json.get(col);
            if (o == null) {
                throw new ParmsException("缺少必要参数:" + col);
            } else {
                if (o instanceof String) {
                    if (((String) o).length() == 0) {
                        throw new ParmsException("参数" + col + "不能为空");
                    }
                }
            }
        }
        return true;
    }

    protected boolean checkParms(Object... objs) throws ParmsException {
        int errorIndex = 0;
        for (Object o : objs) {
            errorIndex++;
            if (o == null) {
                throw new ParmsException(errorIndex);
            } else {
                if (o instanceof String) {
                    if (((String) o).length() == 0) {
                        throw new ParmsException(errorIndex);
                    }
                }
            }
        }
        return true;
    }

    protected boolean checkJavaSpecParams(BindingResult bindingResult) throws ParmsException {
        if (bindingResult.hasErrors()) {
            throw new ParmsException(bindingResult.getFieldError().getDefaultMessage());
        }
        return true;
    }

    protected boolean checkParmsInteger(Object... objs) throws ParmsException {
        int errorIndex = 0;
        for (Object o : objs) {
            errorIndex++;
            if (o == null) {
                throw new ParmsException(errorIndex);
            } else {
                if (o instanceof String) {
                    if (((String) o).length() == 0) {
                        throw new ParmsException(errorIndex);
                    }
                }
                if (o instanceof Integer) {
                    if (((Integer) o) == 0) {
                        throw new ParmsException(errorIndex);
                    }
                }
            }
        }
        return true;
    }

    protected ComResult error(int code, String msg) {
        return error(code, msg, "");
    }

    protected ComResult error(int code, String msg, Object obj) {
        return new ComResult(false, code, msg, obj);
    }

    protected ComResult success(Object obj) {
        return success(RetCode.RetOK_msg, obj);
    }

    protected ComResult success(String msg, Object obj) {
        return success(RetCode.RetOK, msg, obj);
    }

    protected ComResult success(int code, String msg, Object obj) {
        return new ComResult(true, code, msg, obj);
    }
}
