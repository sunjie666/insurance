FROM java:8-jre
MAINTAINER wanglei <wanglei@51haohuo.com>

ADD ./target/securitycenter-0.0.1-SNAPSHOT.jar /app/securitycenter-0.0.1-SNAPSHOT.jar
ADD ./target/classes/logback.xml /app/logback.xml

ENTRYPOINT exec java -server -Xmx512m -Xms512m -Xmn256m -Xss512k -Dsun.jnu.encoding=UTF-8 -Duser.timezone=Asia/Shanghai -Dfile.encoding=UTF-8  -jar /app/securitycenter-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod
EXPOSE 8009
